﻿using IngressLogger.EF.SqlServer;
using IngressLogger.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger
{
    class Program
    {
        static void Main(string[] args)
        {

            var stud = new Portal
            {
                Name = "test2",
                PositionLangE6 = 10,
                PositionLatE6 = 10,
                GUID = "guidtest2",
                TimeStamp = DateTime.Now
            };

            IngressLoggerApp ingressLogger = new IngressLoggerApp();
            ingressLogger.CreatePortal(stud);
            ingressLogger.AddDetailsPortal(stud);
        }
    }
}
