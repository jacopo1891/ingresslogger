﻿using IngressLogger.EF.SqlServer;
using IngressLogger.Entities;
using IngressLogger.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger
{
    public class IngressLoggerApp
    {
        ApplicationContext db;

        public IngressLoggerApp()
        {
            db = new ApplicationContext();
        }

        public Portal CreatePortal(Portal portal)
        {
            Portal portalFound = SearchPortal(portal.GUID, portal.PositionLangE6, portal.PositionLatE6);
            if (portalFound == null)
            {
                Portal p = new Portal
                {
                    GUID = portal.GUID,
                    PositionLangE6 = portal.PositionLangE6,
                    PositionLatE6 = portal.PositionLatE6,
                    TimeStamp = portal.TimeStamp,
                    Faction = portal.Faction
                };
                db.Portals.Add(p);
                db.SaveChanges();
                return p;
            }
            else
            {
                portalFound.TimeStamp = portal.TimeStamp;
                portalFound.Faction = portal.Faction;
                db.SaveChanges();
            }
            return portalFound;
        }

        public Portal SearchPortal(string guid, double langE6, double latE6)
        {
            return db.Portals.FirstOrDefault(p => p.GUID == guid
                                        && p.PositionLangE6 == langE6
                                        && p.PositionLatE6 == latE6);
        }

        public Portal AddDetailsPortal(Portal portalDetail)
        {
            Portal portalFound = SearchPortal(portalDetail.GUID, portalDetail.PositionLangE6, portalDetail.PositionLatE6);
            if (portalFound == null)
            {
                Portal p = new Portal
                {
                    GUID = portalDetail.GUID,
                    PositionLangE6 = portalDetail.PositionLangE6,
                    PositionLatE6 = portalDetail.PositionLatE6,
                    TimeStamp = portalDetail.TimeStamp,
                    Image = portalDetail.Image,
                    Name = portalDetail.Name,
                    Address = portalDetail.Address
                };
                db.Portals.Add(p);
                db.SaveChanges();
                return p;
            }
            else
            {
                portalFound.TimeStamp = portalDetail.TimeStamp;
                portalFound.Image = portalDetail.Image;
                portalFound.Name = portalDetail.Name;
                portalFound.Address = portalDetail.Address;
                db.SaveChanges();
            }
            return portalFound;
        }
    }
}
