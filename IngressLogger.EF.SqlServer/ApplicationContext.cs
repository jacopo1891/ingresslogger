﻿using IngressLogger.DAL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.EF.SqlServer
{
    public class ApplicationContext : EFApplicationContext
    {
        public ApplicationContext() : base("name=ConnectionStringName")
        {
            //Database.SetInitializer<ApplicationContext>(null);
        }
    }
}
