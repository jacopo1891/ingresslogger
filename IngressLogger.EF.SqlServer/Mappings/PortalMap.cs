﻿using IngressLogger.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.EF.SqlServer.Mappings
{
    public class PortalMap : EntityTypeConfiguration<Portal>
    {
        public PortalMap()
        {
            this.ToTable("Portal");
            this.HasKey(x => new { x.GUID, x.PositionLangE6, x.PositionLatE6});
            this.Property(x => x.Id).HasColumnName("PortalId");
            this.Property(x => x.GUID).HasColumnName("GUID");
            this.Property(x => x.PositionLangE6).HasColumnName("LangE6");
            this.Property(x => x.PositionLatE6).HasColumnName("LatE6");
            this.Property(x => x.Name).HasColumnName("Name").IsRequired();
            this.Property(x => x.Address).HasColumnName("Address").IsRequired();
            this.Property(x => x.Image).HasColumnName("Image");
            this.Property(x => x.TimeStamp).HasColumnName("TimeStamp");
            this.Property(x => x.Faction).HasColumnName("Faction");
        }

    }
}
