﻿using IngressLogger.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.EF.SqlServer.Mappings
{
    public class AgentMap : EntityTypeConfiguration<Agent>
    {
        public AgentMap()
        {
            this.ToTable("Agents");
            this.HasKey(x => x.Id);
            this.Property(x => x.Id).HasColumnName("AgentId");
            this.Property(x => x.Nickname).HasColumnName("Nickname").IsRequired();
            this.Property(x => x.Faction).HasColumnName("Faction").IsRequired();
        }
    }
}
