﻿using IngressLogger.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.EF.SqlServer.Mappings
{
    public class GameActionMap : EntityTypeConfiguration<GameAction>
    {
        public GameActionMap()
        {
            this.ToTable("GameActions");
            this.HasKey(x => x.Id);
            this.HasRequired(x => x.GUID);
            this.HasRequired(x => x.Agent);
            this.HasRequired(x => x.PortalA);
            this.HasOptional(x => x.PortalB);
            this.Property(x => x.Id).HasColumnName("GameActionId");
            this.Property(x => x.Action).HasColumnName("Action").IsRequired();
            this.Property(x => x.Date).HasColumnName("Date").IsRequired();
            this.Property(x => x.Message).HasColumnName("Message");
            this.Property(x => x.MUCaptured).HasColumnName("MuCaptured");
        }
    }
}
