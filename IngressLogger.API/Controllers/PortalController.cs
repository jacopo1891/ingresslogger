﻿using IngressLogger.Entities;
using IngressLogger.Entities.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IngressLogger.API.Controllers
{
    [Route("api/portals")]
    public class PortalController : ApiController
    {
        private IngressLoggerApp Logger;
        
        public PortalController(IngressLoggerApp ingressLogger)
        {
            Logger = ingressLogger;
        }

        [HttpPost]
        public HttpResponseMessage CreatePortal(PortalCreateForm portal)
        {
            try
            {
                Portal p = new Portal {
                    GUID = portal.GUID,
                    PositionLangE6 = portal.PositionLangE6,
                    PositionLatE6 = portal.PositionLatE6,
                    TimeStamp = new DateTime(long.Parse(portal.TimeStamp)),
                    Faction = portal.Faction
                };
                Logger.CreatePortal(p);
                return this.Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, e.Message);
            }
        }
    }
}
