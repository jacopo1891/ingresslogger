﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using IngressLogger.Entities;

namespace IngressLogger.DAL.EF
{
    public abstract class EFApplicationContext : DbContext
    {
        public EFApplicationContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<EFApplicationContext>(null);
        }

        public virtual DbSet<Portal> Portals { get; set; }
        public virtual DbSet<Agent> Agents { get; set; }
        public virtual DbSet<GameAction> GameActions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
