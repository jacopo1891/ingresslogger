﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities
{
    public class GameAction
    {
        public long Id { get; set; }
        public string GUID { get; set; }
        public string Action { get; set; }
        public virtual Agent Agent { get; set; }
        public DateTime Date { get; set; }
        public virtual Portal PortalA { get; set; }
        public virtual Portal PortalB { get; set; }
        public int MUCaptured { get; set; }
        public string Message { get; set; }
    }
}
