﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities
{
    public class Agent
    {
        public long Id { get; set; }
        public String Nickname { get; set; }
        public String Faction { get; set; }
        public DateTime FirstSeen { get; set; }
        public DateTime LastSeen { get; set; }
        public virtual ICollection<GameAction> GameActions { get; set; }
    }
}
