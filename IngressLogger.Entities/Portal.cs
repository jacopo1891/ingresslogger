﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities
{
    public class Portal
    {
        public long Id { get; set; }
        public string GUID { get; set; }
        public double PositionLatE6 { get; set; }
        public double PositionLangE6 { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Faction { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        //public virtual ICollection<GameAction> GameActions { get; set; }
    }
}
