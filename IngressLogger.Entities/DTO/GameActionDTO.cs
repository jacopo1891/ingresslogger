﻿using IngressLogger.Entities;
using IngressLogger.Entities.DTO;using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities.DTO
{
    public class GameActionDTO
    {
        public string Action { get; set; }
        public AgentDTO Agent { get; set; }
        public PortalDetailDTO PortalA { get; set; }
        public DateTime Date { get; set; }
        public PortalDetailDTO PortalB { get; set; }
        public int MUCaptured { get; set; }
        public string Message { get; set; }
    }
}
