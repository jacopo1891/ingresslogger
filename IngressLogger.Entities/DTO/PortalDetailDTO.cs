﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities.DTO
{
    public class PortalDetailDTO
    {
        public String GUID { get; set; }
        public String Name { get; set; }
        public String Image { get; set; }
        public String Address { get; set; }
        public string TimeStamp { get; set; }
        public double PositionLatE6 { get; set; }
        public double PositionLangE6 { get; set; }
    }
}
