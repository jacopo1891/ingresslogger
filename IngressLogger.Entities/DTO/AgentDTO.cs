﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities.DTO
{
    public class AgentDTO
    {
        public String Nickname { get; set; }
        public String Faction { get; set; }
    }
}
