﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressLogger.Entities.Forms
{
    public class PortalCreateForm
    {
        public string GUID { get; set; }
        public double PositionLatE6 { get; set; }
        public double PositionLangE6 { get; set; }
        public string TimeStamp { get; set; }
        public string Faction { get; set; }
    }
}
